const {HttpStatusCodes} = global.SnappFramework;

module.exports = function (self) {
  self.response.write(HttpStatusCodes.NOT_FOUND, 'Not Found');
};
