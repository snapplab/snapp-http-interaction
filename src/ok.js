const {HttpStatusCodes, MediaType} = global.SnappFramework;

const getResponse = function () {
  let template = this.optional.Template;
  if (template) {
    return {mediaType: template.mediaType,
            contents: template.contents};
  }

  let obj = this.optional.object;
  if (obj) {
    return {mediaType: MediaType.json,
            contents: JSON.stringify(obj)};
  }

  let str = this.optional.string;
  if (str) {
    return {mediaType: MediaType.text,
            contents: str};
  }

  return {};
}

module.exports = function (self) {
  let {mediaType, contents} = getResponse.call(this);

  if (typeof contents !== 'string') {
    return self.response.write(HttpStatusCodes.SERVER_ERROR, 'Server Error');
  }

  self.response.write(HttpStatusCodes.OK, mediaType, contents);
};
